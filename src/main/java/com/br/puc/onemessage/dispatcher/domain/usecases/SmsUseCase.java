package com.br.puc.onemessage.dispatcher.domain.usecases;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.br.puc.onemessage.dispatcher.data.requests.SmsTwilioRequest;
import com.br.puc.onemessage.dispatcher.data.senders.SmsSender;
import com.br.puc.onemessage.dispatcher.data.senders.impl.TwilioSmsSender;

@Service
public class SmsUseCase {
    private final SmsSender smsSender;

    @Autowired
    public SmsUseCase(@Qualifier("twilio") TwilioSmsSender smsSender) {
        this.smsSender = smsSender;
    }

    public void sendSms(SmsTwilioRequest smsRequest) {
        smsSender.sendSms(smsRequest);
    }
}
