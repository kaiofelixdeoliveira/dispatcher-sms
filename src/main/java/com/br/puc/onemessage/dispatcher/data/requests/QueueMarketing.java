package com.br.puc.onemessage.dispatcher.data.requests;

public class QueueMarketing {
	private String message;
	private String phone;

	public QueueMarketing() {
		super();
	}

	public QueueMarketing(String message, String phone) {
		super();
		this.message = message;
		this.phone = phone;

	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "QueueMarketing [message=" + message + ", phone=" + phone + "]";
	}

}
