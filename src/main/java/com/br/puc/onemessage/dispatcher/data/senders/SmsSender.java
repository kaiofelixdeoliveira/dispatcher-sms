package com.br.puc.onemessage.dispatcher.data.senders;

import com.br.puc.onemessage.dispatcher.data.requests.SmsTwilioRequest;

public interface SmsSender {

    void sendSms(SmsTwilioRequest smsRequest);

    // or maybe void sendSms(String phoneNumber, String message);
}