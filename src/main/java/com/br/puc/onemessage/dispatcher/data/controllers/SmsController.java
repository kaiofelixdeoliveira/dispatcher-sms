package com.br.puc.onemessage.dispatcher.data.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.br.puc.onemessage.dispatcher.data.requests.SmsTwilioRequest;
import com.br.puc.onemessage.dispatcher.domain.usecases.SmsUseCase;

import javax.validation.Valid;

@RestController
@CrossOrigin("*")
@RequestMapping("/sms")
public class SmsController {

    private final SmsUseCase smsService;

    @Autowired
    public SmsController(SmsUseCase smsService) {
        this.smsService = smsService;
    }

    @PostMapping
    public void sendSms(@Valid @RequestBody SmsTwilioRequest smsRequest) {
        smsService.sendSms(smsRequest);
    }
    

	@GetMapping
	public String sayHello() {

		return "Dispatcher Microservice 20!";
	}
}
