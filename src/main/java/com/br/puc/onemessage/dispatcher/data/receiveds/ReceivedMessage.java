package com.br.puc.onemessage.dispatcher.data.receiveds;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.br.puc.onemessage.dispatcher.data.requests.QueueCorporateRequest;
import com.br.puc.onemessage.dispatcher.data.requests.SmsTwilioRequest;
import com.br.puc.onemessage.dispatcher.domain.usecases.SmsUseCase;

@Component
public class ReceivedMessage {
	
	private static final String QUEUE_NAME = "corporate-queue";
	private final SmsUseCase smsService;
	private final Logger logger = LoggerFactory.getLogger(ReceivedMessage.class);
	
	@Autowired
    public ReceivedMessage(SmsUseCase smsService) {
        this.smsService = smsService;
    }

	@JmsListener(destination = QUEUE_NAME, containerFactory = "listenerContainerFactory")
	public void receiveMessage(QueueCorporateRequest request) {
		logger.info("Received message: {}", request.getMessage());
		 smsService.sendSms(new SmsTwilioRequest(request.getPhone(),request.getMessage()));
	}

}
