package com.br.puc.onemessage.dispatcher.data.requests;

public class QueueInteractive {
	private String message;
	private String phone;

	public QueueInteractive() {
		super();
	}

	public QueueInteractive(String message, String phone) {
		super();
		this.message = message;
		this.phone = phone;

	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "QueueInteractive [message=" + message + ", phone=" + phone + "]";
	}

}
